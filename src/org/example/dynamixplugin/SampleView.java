/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.example.dynamixplugin;

import java.io.IOException;
import java.net.URL;

import org.ambientdynamix.api.contextplugin.ActivityController;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.IPluginView;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Example IPluginView that provides a simple user interface for the plug-in. </br>
 * 
 * IPluginView implementations are used to create user interfaces that your plug-in might use. For For example, your
 * plug-in may provide a IPluginView that is returned when your runtime has its 'getConfigurationView' method called.
 * Additionally, your plug-in may open a IPluginView in response to having its 'handleContextRequest' or
 * 'handleConfiguredContextRequest' method called. In these cases, your runtime will need to call the 'openView' method
 * of the ContextPluginRuntime base class, providing the IPluginView implementation to show.
 * 
 * @author Darren Carlson
 *
 */
public class SampleView implements IPluginView {
	private String TAG = this.getClass().getSimpleName();
	private Context context;
	private ActivityController controller;
	private String message;
	private ContextPluginRuntime runtime;

	/**
	 * Ceates a SampleView.
	 * 
	 * @param context
	 *            The runtime's context.
	 * @param message
	 *            The message to display.
	 */
	public SampleView(Context context, ContextPluginRuntime runtime, String message) {
		this.context = context;
		this.runtime = runtime;
		this.message = message;
	}

	/**
	 * Sets the activity controller, which can be used to close the host activity. This method will be called before all
	 * others.
	 */
	@Override
	public void setActivityController(ActivityController controller) {
		this.controller = controller;
	}

	/**
	 * Returns the view's preferred orientation. An orientation constant as used in ActivityInfo.<screenOrientation>.
	 * For example, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE. For details, see
	 * http://developer.android.com/reference/android/R.attr.html#screenOrientation.
	 */
	@Override
	public int getPreferredOrientation() {
		return ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
	}

	/**
	 * Relays Intents received by the HostActivity to this IPluginView.
	 */
	@Override
	public void handleIntent(Intent intent) {
		// Not used
	}

	/**
	 * Returns the View to be injected into the host activity. In this example, we construct a simple LinearLayout with
	 * a button and image (loaded from the plug-ins asset folder). We also demonstrate how to access some display
	 * metrics and language preferences.
	 */
	@Override
	public View getView() {
		// Discover our screen size for proper formatting
		DisplayMetrics met = context.getResources().getDisplayMetrics();
		Log.i(TAG, "Screen Size (WxH): " + met.widthPixels + "x" + met.heightPixels);
		// Access our Locale via the incoming context's resource configuration to determine language
		String language = context.getResources().getConfiguration().locale.getDisplayLanguage();
		Log.i(TAG, "Languate: " + language);
		// Create a layout
		LinearLayout lLayout = new LinearLayout(context);
		lLayout.setOrientation(LinearLayout.VERTICAL);
		lLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		// Create a text view
		TextView tView = new TextView(context);
		tView.setText(message);
		tView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		lLayout.addView(tView);
		// Create a button
		final Button flashToggle = new Button(context);
		flashToggle.setText("Close Me!");
		flashToggle.setMinimumWidth(150);
		flashToggle.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				/*
				 * Here, we would validate the configuration view and set out runtime as configured (if needed)
				 */
				runtime.setPluginConfiguredStatus(true);
				/*
				 * Next, we use the activity controller to close the Host Activity provided by Dynamix.
				 */
				controller.closeActivity();
			}
		});
		lLayout.addView(flashToggle);
		/*
		 * This section loads a sample image from the plug-in's asset folder into an ImageView.
		 */
		ImageView image = new ImageView(context);
		try {
			// Grab the plug-in's ClassLoader using the SecuredContext
			ClassLoader classLoader = context.getClassLoader();
			if (classLoader != null) {
				// Create a URL to the resource you want
				URL url = classLoader.getResource("assets/agt_web.png");
				if (url != null) {
					// Load the image into a Bitmap using the BitmapFactory
					Bitmap bm = BitmapFactory.decodeStream(url.openStream());
					image.setImageBitmap(bm);
					// Add the Bitmap to the layout
					lLayout.addView(image);
				} else {
					Log.w(TAG, "URL was null");
				}
			}
		} catch (IOException e) {
			Log.e(TAG, e.toString());
		}
		return lLayout;
	}

	/**
	 * Specifies that the view should release any acquired resources and prepare for garbage collection. Note that if a
	 * context request is ongoing, the underlying runtime should call "sendContextRequestError" with the reason
	 * REQUEST_CANCELLED; for example: runtime.sendContextRequestError(requestId, "Barcode Scan: REQUEST_CANCELLED",
	 * ErrorCodes.REQUEST_CANCELLED);
	 */
	@Override
	public void destroyView() {
		controller.closeActivity();
	}
}
