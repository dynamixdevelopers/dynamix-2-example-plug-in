/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.example.dynamixplugin;

import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.UUID;

import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.IMessageResultHandler;
import org.ambientdynamix.api.contextplugin.IPluginView;
import org.ambientdynamix.api.contextplugin.Message;
import org.ambientdynamix.api.contextplugin.PluginConstants;
import org.ambientdynamix.api.contextplugin.PowerScheme;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

/**
 * Example Dynamix plug-in that detects the device's battery level and handles sample commands. The plug-in also
 * demonstrates how to specify and use configuration data in the 'handleConfiguredContextRequest' method. Additional
 * plug-in functionality is also demonstrated, including features, configuration views, storing settings and setting
 * configured status (see 'additionalMethodExamples').
 * 
 * @author Darren Carlson
 * 
 */
public class PluginRuntime extends ContextPluginRuntime {
	/*
	 * Public static variables for our supported context types. These must be defined using the 'supportedContextTypes'
	 * element in the plug-ins metadata.
	 */
	public static String CONTEXT_TYPE_BATTERY = "org.example.dynamixplugin.batterylevel";
	public static String CONTEXT_TYPE_COMMAND = "org.example.dynamixplugin.commandtest";
	// The validity duration of context results
	private static final int VALID_CONTEXT_DURATION = 60000;
	// Static logging TAG
	private final String TAG = this.getClass().getSimpleName();
	// Our secure context
	private Context context;

	/**
	 * Called once when the ContextPluginRuntime is first initialized. Within this method, the plug-in should acquire
	 * the resources necessary to run; however, it should not start. If initialization is unsuccessful, the plug-ins
	 * should throw an exception and release any acquired resources.
	 */
	@Override
	public void init(PowerScheme powerScheme, ContextPluginSettings settings) throws Exception {
		// Set the power scheme
		this.setPowerScheme(powerScheme);
		// Store our secure context
		this.context = this.getSecuredContext();
	}

	/**
	 * Called by the Dynamix Context Manager to start the plug-in.
	 */
	@Override
	public void start() {
		/*
		 * Send the current battery level immediately. Note that passing null into 'registerReceiver' provides an
		 * immediate battery level result. We broadcast this result to all Dynamix context subscribers that have
		 * registered for the context type 'org.ambientdynamix.contextplugins.batterylevel'.
		 */
		Intent batteryIntent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
		sendBroadcastContextEvent(new BatteryLevelInfo(batteryIntent), VALID_CONTEXT_DURATION);
		/*
		 * Also register for ongoing battery level changed notifications using the 'batteryLevelReceiver'.
		 */
		context.registerReceiver(batteryLevelReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
		Log.d(TAG, "Started!");
	}

	/**
	 * Called by the Dynamix Context Manager to stop operations. Acquired resources should be maintained, since start
	 * may be called again at any time.
	 */
	@Override
	public void stop() {
		// Unregister battery level changed notifications
		context.unregisterReceiver(batteryLevelReceiver);
		Log.d(TAG, "Stopped!");
	}

	/**
	 * Stops the runtime and releases all acquired resources in preparation for garbage collection. Once this method has
	 * been called, this plug-in instance will not be re-started and will be reclaimed by garbage collection sometime in
	 * the indefinite future.
	 */
	@Override
	public void destroy() {
		this.stop();
		context = null;
		Log.d(TAG, "Destroyed!");
	}

	/**
	 * Handles contexts request from an app. In this example, we return the battery state and details by sending a
	 * BatteryLevelInfo object using the 'sendContextEvent' method of the runtime base class.
	 */
	@Override
	public void handleContextRequest(UUID requestId, String contextType) {
		// Check for CONTEXT_TYPE_BATTERY
		if (contextType.equalsIgnoreCase(CONTEXT_TYPE_BATTERY)) {
			// Manually access the battery level with a null BroadcastReceiver
			Intent batteryIntent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
			/*
			 * Return the battery level result to the requesting application, using the incoming requestId as the
			 * responseId in the 'sendContextEvent' method.
			 */
			sendContextEvent(requestId, new BatteryLevelInfo(batteryIntent), VALID_CONTEXT_DURATION);
		} else {
			sendContextRequestError(requestId, "handleContextRequest cannot support " + contextType,
					ErrorCodes.CONTEXT_SUPPORT_NOT_FOUND);
		}
	}

	/**
	 * Handle configured context requests from apps. In this example, we demonstrate how to handle configuration options
	 * from client from both native apps and Web apps. Plug-in developers are free to define the key/value pairs that
	 * apps should provide in the config Bundle. Make sure to carefully document the key/value pairs that your plug-in
	 * expects. Together, context types and config Bundles provide the API to your plug-in.
	 */
	@Override
	public void handleConfiguredContextRequest(UUID requestId, String contextType, Bundle config) {
		// Check for CONTEXT_TYPE_COMMAND
		if (contextType.equalsIgnoreCase(CONTEXT_TYPE_COMMAND)) {
			// Make the app provided a config Bundle
			if (config != null) {
				/*
				 * If your plug-in requires config key/value pairs that are not strings, you'll need to provide special
				 * handling for Web requests. Since Web requests can only contain string-based key/value pairs, you'll
				 * need to convert incoming Web request values to the config data types expected by your plug-in. To
				 * make things easier, it's generally recommended to specify only string-based key/value pairs for the
				 * config Bundle, which will allow your plug-in to work with native apps and Web apps in the same way.
				 * See the 'logWebAgentRequestInfo' for details on accessing all Web request details.
				 */
				if (config.getBoolean(PluginConstants.WEB_REQUEST)) {
					// Log all data about Web requests
					logWebAgentRequestInfo(config);
				}
				/*
				 * For this example, we specify that apps making configured context requests must set a 'requestCommand'
				 * key in the config Bundle with a value of 'sendSuccess' or 'echoRequest'. The code below provides
				 * example behavior for each command type. Note that your plug-in may specify any key/value pairs
				 * required for a context request (i.e., 'requestCommand' is only used as an example here).
				 */
				String command = config.getString("requestCommand");
				// Handle each command type
				switch (command) {
				case "sendSuccess":
					/*
					 * If the client sends 'sendSuccess', we reply by simply sending a success result.
					 */
					sendContextRequestSuccess(requestId);
					break;
				case "echoRequest":
					/*
					 * If the client sends 'echoRequest', we create a response Bundle containing the incoming echo text
					 * and send it back to the calling app. This example demonstrates the use of the BundleContextInfo,
					 * which is automatically generated by Dynamix when calling 'sendContextEvent' with a Bundle. For
					 * more complex context event data, it's recommended to create your own IContextInfo implementation
					 * (see this project's BatteryLevelInfo class for an example).
					 */
					String echoText = config.getString("echoText", "No echo text provided!");
					Bundle responseBundle = new Bundle();
					responseBundle.putString("echoResponse", echoText);
					sendContextEvent(requestId, responseBundle, contextType);
					break;
				/*
				 * At this point, your could define other command strings that could be handled by your plug-in and
				 * handle each appropriately.
				 */
				default:
					/*
					 * No supported command found, so send back an error.
					 */
					sendContextRequestError(requestId, "No command found!", ErrorCodes.CONFIGURATION_ERROR);
					break;
				}
			} else {
				/*
				 * No config found, so send back an error.
				 */
				sendContextRequestError(requestId, "No configuration found!", ErrorCodes.MISSING_CONFIGURATION);
			}
		} else {
			sendContextRequestError(requestId, "handleConfiguredContextRequest cannot support " + contextType,
					ErrorCodes.CONTEXT_SUPPORT_NOT_FOUND);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateSettings(ContextPluginSettings settings) {
		// This method is deprecated... do not use.
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPowerScheme(PowerScheme scheme) {
		// Adjust plug-in performance to match the incoming power scheme
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IPluginView getDefaultConfigurationView() {
		// Return the SampleView
		return new SampleView(context, this, "Default configuration view.");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IPluginView getConfigurationView(Bundle viewConfig) {
		if (viewConfig != null)
			return new SampleView(context, this, "Specific configuration view for config key CONFIG_TYPE "
					+ viewConfig.getString("CONFIG_TYPE"));
		else {
			return new SampleView(context, this, "Specific configuration view without a config key.");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onMessageReceived(Message message) {
		/*
		 * Receive message. Requires 'Permissions.RECEIVE_INTER_PLUGIN_MESSAGES' be set in the plug-in's metadata. Note
		 * that the message format is defined by the sender plug-in (see plug-in's documentation for details).
		 */
		Log.i(TAG, "Message received from " + message.getSender().getPluginId() + ": " + message.getMessage());
		// Check for result handler
		if (message.hasResultHandler()) {
			// Send result
			Bundle result = new Bundle();
			result.putString("RESULT_KEY", "Results generated at: " + new Date().toString());
			message.getResultHandler().onResult(result);
		}
		/*
		 * Sends message to the latest plug-in identified by the plug-in id. To send to a specific version of the
		 * plug-in, use the 'sendMessage' with version support. Requires 'Permissions.SEND_INTER_PLUGIN_MESSAGES' be set
		 * in the plug-in's metadata. Note that the message format is defined by the sender plug-in (see plug-in's
		 * documentation for details).
		 */
		Bundle sendMessage = new Bundle();
		sendMessage.putString("MESSAGE_KEY", "Message generated at: " + new Date().toString());
		sendMessage("target.plugin.id", sendMessage, new IMessageResultHandler() {
			@Override
			public void onResult(Bundle result) {
				Log.i(TAG, "Message result: " + result);
			}

			@Override
			public void onFailure(String message, int errorCode) {
				Log.w(TAG, "Message failure: " + message);
			}
		});
	}

	/**
	 * Example of a "Dynamix Feature", which is exposed by this plug-in. Features are registered by listing the
	 * feature's runtime method (i.e., 'runFeature' in this case) in the plug-ins xml metadata. See the example metadata
	 * file for this project in the "Deployment" directory.
	 */
	public void runFeature() {
		/*
		 * In this example, we log to DDMS and open a sample view
		 */
		Log.i(TAG, "This is an example of a feature launched from within Dynamix: " + new Date());
		openView(new SampleView(context, this, "This view was opened by a feature!"));
	}

	// A BroadcastReceiver variable that is used to receive battery status updates from Android
	private BroadcastReceiver batteryLevelReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			/*
			 * When we receive a battery level event from Android, we broadcast the data to all Dynamix context
			 * subscribers that have registered for the context type 'org.ambientdynamix.contextplugins.batterylevel'.
			 */
			sendBroadcastContextEvent(new BatteryLevelInfo(intent), VALID_CONTEXT_DURATION);
		}
	};

	/*
	 * Utility method that logs information provided by Web agent during configured context requests.
	 */
	private void logWebAgentRequestInfo(Bundle config) {
		// Log Web data
		Log.i(TAG, "Configured Context Request from a Web Agent!");
		Log.i(TAG, "Web Request Method: " + config.getString(PluginConstants.WEB_REQUEST_METHOD));
		Log.i(TAG, "Web Request URI: " + config.getString(PluginConstants.WEB_REQUEST_URI));
		Log.i(TAG, "Web Request Content Type: " + config.getString(PluginConstants.WEB_REQUEST_CONTENT_TYPE));
		Log.i(TAG, "Web Request Content Length: " + config.getString(PluginConstants.WEB_REQUEST_CONTENT_LENGTH));
		// Log request content using Java Properties
		if (config.containsKey(PluginConstants.WEB_REQUEST_CONTENT)) {
			/*
			 * The content of the web agent request will be contained within a Java Properties object as strings. Here,
			 * we iterate over the property names and log the values.
			 */
			Properties parms = (Properties) config.getSerializable(PluginConstants.WEB_REQUEST_CONTENT);
			Enumeration<?> e = parms.propertyNames();
			while (e.hasMoreElements()) {
				String key = (String) e.nextElement();
				Log.i(TAG, "Web Request Properties Key " + key + " contained value " + parms.getProperty(key));
			}
		}
		/*
		 * Log request content using Bundle config itself. For convenience, Dynamix exports all request content as
		 * key/value string pairs that can be directly accessed within the config Bundle. This data will be the same as
		 * the content Java Properties above... just easier to access.
		 */
		for (String key : config.keySet())
			Log.i(TAG, "Web Request Config Bundle Key " + key + " contained value " + config.getString(key));
	}

	/*
	 * Examples of how to store/retrieve settings and set configured status.
	 */
	private void additionalMethodExamples() {
		/*
		 * Plug-ins can store string-based settings using the Dynamix framework, as follows:
		 */
		ContextPluginSettings newSettings = new ContextPluginSettings();
		newSettings.put("SETTINGS_KEY", "Settings value");
		storeContextPluginSettings(newSettings);
		/*
		 * You can also retrieve your settings at any time as follows (Note that previously stored settings will be
		 * passed to the plug during 'init'):
		 */
		ContextPluginSettings storedSettings = getContextPluginSettings();
		/*
		 * If your plug-in requires configuration before it can operate, you should declare requiresConfiguration="true"
		 * in the plug-in's metadata. Once your plug-in has been configured (e.g., through a configuration UI), you
		 * should use the 'requiresConfiguration' to set the plug-in's configured status to true. Unconfigured plug-ins
		 * will not be started by Dynamix. You can set the plug-in's configuration status to 'configured' to true
		 * follows:
		 */
		getPluginFacade().setPluginConfiguredStatus(getSessionId(), true);
		// Log the Dynamix version
		Log.i(TAG, "Dynamix Version: " + getDynamixVersion());
		// Log info about installed plug-ins
		for (ContextPluginInformation plug : getInstalledContextPluginInformation())
			Log.i(TAG, "Installed plug-in: " + plug);
	}
}